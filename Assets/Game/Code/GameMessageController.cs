﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMessageController : MonoBehaviour
{
    [System.Serializable]
    public struct GameMessageData
    {
        public string title;
        public string subtitle;
        public float duration;
        public EGameMessageType messageType;
    };

    public enum EGameMessageType
    {
        Victory,
        GameOver,
        Preparation
    };

    [SerializeField]
    private RectTransform messageParentTransform;
    [SerializeField]
    private Text titleMessageText;
    [SerializeField]
    private Text subtitleMessageText;

    [SerializeField]
    private GameMessageData[] messageTypes;


    private float elapsedTime;
    private float currentTargetDuration;
    private bool isVisible = false;

    private Dictionary<EGameMessageType, GameMessageData> messageMap = new Dictionary<EGameMessageType, GameMessageData>(3);


    void Awake()
    {
        Hide();

        foreach (var message in messageTypes)
        {
            if (!messageMap.ContainsKey(message.messageType))
            {
                messageMap.Add(message.messageType,
                    new GameMessageData()
                    {
                        messageType = message.messageType,
                        title = message.title,
                        subtitle = message.subtitle,
                        duration = message.duration
                    });
            }
        }
    }

    void Update()
    {
        if (isVisible)
        {
            if (currentTargetDuration > 0)
            {
                elapsedTime += Time.deltaTime;
                if (elapsedTime >= currentTargetDuration)
                {
                    Hide();
                }
            }
        }
    }

    public void ShowMessage(EGameMessageType messageType)
    {
        if (messageMap.ContainsKey(messageType))
        {
            ShowMessage(messageMap[messageType]);
        }
    }

    public void ShowMessage(GameMessageData message)
    {
        ShowMessage(message.title, message.subtitle, message.duration);
    }

    public void ShowMessage(string title, string subtitle, float duration)
    {
        titleMessageText.text = title;
        subtitleMessageText.text = subtitle;
        currentTargetDuration = duration;

        isVisible = true;
        messageParentTransform.gameObject.SetActive(isVisible);
    }

    public void Hide()
    {
        isVisible = false;
        messageParentTransform.gameObject.SetActive(isVisible);
    }
}
