﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    public class PlayerCameraController : MonoBehaviour
    {
        public enum ECameraMode
        {
            FirstPerson,
            ThirdPerson
        };

        private ECameraMode currentCameraMode;

        [SerializeField]
        private Transform FirstPersonCameraPivot;
        [SerializeField]
        private Transform ThirdPersonCameraPivot;


        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (currentCameraMode == ECameraMode.FirstPerson)
                    currentCameraMode = ECameraMode.ThirdPerson;
                else
                    currentCameraMode = ECameraMode.FirstPerson;

                SetCameraMode(currentCameraMode);
            }
        }

        public void SetCameraMode(ECameraMode cameraMode)
        {
            currentCameraMode = cameraMode;

            UpdateCameraAttach();
        }

        private void UpdateCameraAttach()
        {
            switch (currentCameraMode)
            {
                case ECameraMode.FirstPerson:
                    transform.parent = FirstPersonCameraPivot;
                    break;
                case ECameraMode.ThirdPerson:
                    transform.parent = ThirdPersonCameraPivot;
                    break;
            }

            transform.localPosition = Vector3.zero;
        }

        public string GetCurrentCameraName()
        {
            switch (currentCameraMode)
            {
                case ECameraMode.FirstPerson:
                    return "First Person";
                case ECameraMode.ThirdPerson:
                    return "Third Person";
            }

            return string.Empty;
        }
    }
}
