﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField]
        private int Health = 100;

        [SerializeField]
        private int damage = 10;

        public float rotationSpeed = 100.0f;

        public Transform attachParent;
        public Transform spinPivot;

        public float timeBetweenFlips = 0.1f;

        protected float timeSinceLastFlip = 0.0f;

        public float Size = 1f;

        protected bool isRotating = false;
        protected float rotationAccumulator = 0f;

        public delegate void OnKill(bool playerWasKiller);
        public event OnKill OnKilled;

        protected float nextTargetRotationAngle;

        protected Renderer meshRenderer;

        private Color startingColor;
        private int startingHealth;


        public int Damage
        {
            get { return damage; }
        }

        void Awake()
        {
            transform.localScale = Vector3.one * Size;
        }

        void Start()
        {
            timeSinceLastFlip = timeBetweenFlips * 2;

            Vector3 pos = transform.position;
            pos.y = Size * 0.5f;
            transform.position = pos;

            meshRenderer = GetComponentInChildren<Renderer>();

            startingColor = meshRenderer.material.color;

            startingHealth = Health;
        }

        void Update()
        {
            if (!isRotating)
            {
                timeSinceLastFlip += Time.deltaTime;
                if (timeSinceLastFlip >= timeBetweenFlips)
                {
                    StartFlip();
                }
            }
            else
            {
                HandleFlip();
            }
        }

        protected virtual Vector3 CalculateFlipPivotLocation()
        {
            return transform.position + attachParent.TransformDirection(new Vector3(0.0f, -Size * 0.5f, Size * 0.5f));
        }

        protected virtual void StartFlip()
        {
            spinPivot.position = CalculateFlipPivotLocation();

            spinPivot.localRotation = Quaternion.identity;

            Vector3 oldLocation = transform.position;
            transform.parent = spinPivot;
            transform.position = oldLocation;

            isRotating = true;
            rotationAccumulator = 0f;

            nextTargetRotationAngle = 90.0f;
        }

        protected virtual void HandleFlip()
        {
            if (rotationAccumulator >= 90.0f)
            {
                isRotating = false;

                Vector3 rot = spinPivot.rotation.eulerAngles;
                rot.x = nextTargetRotationAngle;
                spinPivot.rotation = Quaternion.Euler(rot);

                transform.parent = attachParent;

                timeSinceLastFlip = 0.0f;
            }
            else
            {
                float rotationAngle = rotationSpeed * Time.deltaTime;
                spinPivot.Rotate(Vector3.right, rotationAngle);
                rotationAccumulator += rotationAngle;
            }
        }

        public void TakeDamage(int amount)
        {
            Health -= amount;

            float delta = (float)Health / startingHealth;
            Color lerpedColor = Color.Lerp(startingColor, Color.black, 1 - delta);
            meshRenderer.material.SetColor("_Color", lerpedColor);

            if (!IsAlive())
            {
                Kill(true);
            }
        }

        public void Kill(bool playerWasKiller)
        {
            Health = 0;

            if (OnKilled != null)
            {
                OnKilled(playerWasKiller);
            }

            Destroy(attachParent.gameObject);
        }

        public bool IsAlive()
        {
            return Health > 0;
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                PlayerController player = other.gameObject.GetComponent<PlayerController>();
                if (player)
                {
                    if (player.IsAlive())
                    {
                        player.TakeDamage(damage);
                        Kill(false);
                    }
                }
            }
        }
    }
}
