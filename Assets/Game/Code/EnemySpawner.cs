﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    public class EnemySpawner : MonoBehaviour
    {
        public float spawnRadius = 100.0f;
        public float timeBetweenSpawns = 0.5f;
        public int maxAliveCubes = 50;

        public int numKillsBeforeTitanSpawn = 5;

        public Transform player;

        public GameObject simpleCubePrefab;
        public GameObject jumpingCubePrefab;
        public GameObject zigZagCubePrefab;
        public GameObject titanCubePrefab;

        private float timeSinceLastSpawn = 0.0f;
        private int numCubeKills = 0;

        private bool isSpawningEnable = true;

        private int aliveCubeCount = 0;


        public delegate void EnemySpawnEvent();
        public event EnemySpawnEvent OnCubeSpawned;
        public event EnemySpawnEvent OnCubeKilled;

	
	    void Update()
	    {
            timeSinceLastSpawn += Time.deltaTime;
            if (timeSinceLastSpawn >= timeBetweenSpawns)
            {
                if (CanSpawnNewCube())
                {
                    int randomSpawnValue = Random.Range(1, 10);

                    if (randomSpawnValue > 0 && randomSpawnValue < 6)
                    {
                        SpawnEnemy(simpleCubePrefab);

                    }
                    else if (randomSpawnValue > 5 && randomSpawnValue < 8)
                    {
                        SpawnEnemy(jumpingCubePrefab);

                    }
                    else
                    {
                        SpawnEnemy(zigZagCubePrefab);
                    }
                }
            }
	    }

        bool CanSpawnNewCube()
        {
            return isSpawningEnable && (aliveCubeCount < maxAliveCubes);
        }
        
        Enemy SpawnEnemy(GameObject enemyCubePrefab)
        {
            Vector3 spawnLocation = Random.insideUnitSphere;
            spawnLocation.y = 0.0f;
            spawnLocation.Normalize();
            spawnLocation *= spawnRadius;

            Vector3 dir = player.position - spawnLocation;
            dir.Normalize();

            Enemy newEnemy = Instantiate(enemyCubePrefab, spawnLocation, Quaternion.LookRotation(dir, Vector3.up)).GetComponentInChildren<Enemy>();
            if (newEnemy)
            {
                newEnemy.OnKilled += OnEnemyCubeKilled;
            }

            aliveCubeCount++;

            timeSinceLastSpawn = 0.0f;

            if (OnCubeSpawned != null)
                OnCubeSpawned();

            return newEnemy;
        }

        void OnEnemyCubeKilled(bool playerWasKiller)
        {
            aliveCubeCount--;

            // only count kill if the player was the killer
            if (playerWasKiller)
            {
                numCubeKills++;
            }

            if (isSpawningEnable)
            {
                if (numCubeKills >= numKillsBeforeTitanSpawn)
                {
                    Enemy titan = SpawnEnemy(titanCubePrefab);
                    isSpawningEnable = false;

                    titan.OnKilled += OnTitanKilled;
                }
            }

            if (OnCubeKilled != null)
                OnCubeKilled();
        }

        void OnTitanKilled(bool playerWasKiller)
        {
            if (playerWasKiller)
            {
                GameModeManager.Instance.NotifyTitanWasKilled();
            }
        }

        public int GetNumKillsBeforeTitanSpawn()
        {
            return numKillsBeforeTitanSpawn - numCubeKills;
        }
    }
}
