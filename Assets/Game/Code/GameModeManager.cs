﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CubicAssault
{
    public class GameModeManager : MonoBehaviour
    {
        public PauseMenuController pauseMenuController;
        public PlayerController playerController;
        public GameMessageController gameMessageController;
        public UIController uiController;

        public Canvas gameOverScreenCanvas;
        public Canvas gameWinScreenCanvas;

        public Camera EndGameCamera;

        private bool isGamePaused = false;

        private static GameModeManager instance;
        public static GameModeManager Instance
        {
            get
            {
                return instance;
            }
        }

        public bool IsGamePaused
        {
            get { return isGamePaused; }
        }


        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);
        }

        void Start()
        {
            Time.timeScale = 1.0f;

            if (pauseMenuController)
            {
                pauseMenuController.OnPaused += OnGamePaused;
                pauseMenuController.OnResumed += OnGameResumed;
            }

            if (playerController)
            {
                playerController.OnKilled += OnPlayerKilled;
            }

            gameWinScreenCanvas.enabled = false;
            gameOverScreenCanvas.enabled = false;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            gameMessageController.ShowMessage(GameMessageController.EGameMessageType.Preparation);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (pauseMenuController && !isGamePaused)
                {
                    pauseMenuController.Pause();
                }
            }
        }

        void OnGamePaused()
        {
            isGamePaused = true;

            uiController.SetCursorUIMode();
        }

        void OnGameResumed()
        {
            isGamePaused = false;

            uiController.SetCursorGameMode();
        }

        void OnPlayerKilled()
        {
            isGamePaused = true;

            EndGame(false);
        }

        public void Restart()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }

        void EndGame(bool victory)
        {
            Time.timeScale = 0.0f;
            isGamePaused = true;

            gameOverScreenCanvas.enabled = !victory;
            gameWinScreenCanvas.enabled = victory;

            gameMessageController.ShowMessage(victory ? GameMessageController.EGameMessageType.Victory : GameMessageController.EGameMessageType.GameOver);

            uiController.SetCursorUIMode();
            uiController.ShowScreen(victory ? UIController.EScreenType.Win : UIController.EScreenType.GameOver);

            EnableEndGameCamera();
        }

        public void NotifyTitanWasKilled()
        {
            EndGame(true);
        }

        private void EnableEndGameCamera()
        {
            Camera.main.enabled = false;
            EndGameCamera.enabled = true;
        }
    }
}
