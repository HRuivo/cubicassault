﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuController : MonoBehaviour
{
    public Canvas PauseMenuCanvas;
    public Canvas GameHUDCanvas;

    public delegate void OnPauseAction();
    public event OnPauseAction OnPaused;
    public event OnPauseAction OnResumed;


    void Start()
    {
        PauseMenuCanvas.enabled = false;
    }

    public void Resume()
    {
        Time.timeScale = 1.0f;
        PauseMenuCanvas.enabled = false;
        GameHUDCanvas.enabled = true;

        if (OnResumed != null)
            OnResumed();
    }

    public void Pause()
    {
        Time.timeScale = 0.0f;
        PauseMenuCanvas.enabled = true;
        GameHUDCanvas.enabled = false;

        if (OnPaused != null)
            OnPaused();
    }

    public void QuitToMainMenu()
    {
        Time.timeScale = 1.0f;
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
