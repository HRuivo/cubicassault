﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        protected int health = 100;

        [SerializeField]
        private PlayerCameraController.ECameraMode startingCameraMode;

        [SerializeField]
        protected Vector2 aimRotationSpeed = Vector2.one * 2.0f;

        [SerializeField]
        private float minPitchAngle = -50.0f;
        [SerializeField]
        private float maxPitchAngle = 45.0f;

        private Weapon currentWeapon;

        [SerializeField]
        private Transform rotationPivot;

        [SerializeField]
        private Transform cannonPivot;

        [SerializeField]
        private Transform weaponPoint;

        private float pitch;
        private float yaw;

        [SerializeField]
        private PlayerCameraController cameraController;

        Vector3 targetAim;

        public Transform head;

        public delegate void PlayerKillAction();
        public delegate void PlayerDamageAction(int damage);

        public event PlayerKillAction OnKilled;
        public event PlayerDamageAction OnDamageReceived;

        private int currentWeaponIndex = 0;
        public Weapon[] weaponList;
        private List<Weapon> inventory;

        private int lastMouseScrollDelta = 0;


        public int Health
        {
            get { return health; }
        }


        void Start()
	    {
            pitch = 0f;
            yaw = 0f;

            if (cameraController)
            {
                cameraController.SetCameraMode(startingCameraMode);
            }

            CreateWeapons();

            currentWeapon = inventory[0];
            currentWeapon.Equip();
	    }

        void CreateWeapons()
        {
            inventory = new List<Weapon>(weaponList.Length);

            for (int i = 0; i < weaponList.Length; i++)
            {
                inventory.Add(Instantiate(weaponList[i].gameObject, weaponPoint).GetComponent<Weapon>());
            }
        }
	
	    void Update()
	    {
            if (GameModeManager.Instance.IsGamePaused)
            {
                return;
            }

            HandleWeaponSelection();

            HandleCameraAiming();

            HandleWeaponFiring();
        }

        void HandleWeaponFiring()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                currentWeapon.StartFiring();
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                currentWeapon.StopFiring();
            }

            Vector3 cameraDir = cameraController.transform.forward;
            Vector3 cameraPos = cameraController.transform.position;

            RaycastHit hit;
            if (Physics.Raycast(cameraPos, transform.TransformDirection(cameraDir), out hit))
            {
                targetAim = hit.point;
                Debug.DrawRay(cameraPos, transform.TransformDirection(cameraDir) * hit.distance, Color.yellow);
            }
            else
            {
                targetAim = transform.TransformDirection(cameraDir) * 10000.0f;
            }

            cannonPivot.LookAt(targetAim, Vector3.up);

            head.eulerAngles = new Vector3(0.0f, cameraController.transform.rotation.eulerAngles.y, 0.0f);
        }

        void HandleCameraAiming()
        {
            pitch += aimRotationSpeed.x * -Input.GetAxis("Mouse Y");
            yaw += aimRotationSpeed.y * Input.GetAxis("Mouse X");

            pitch = Mathf.Clamp(pitch, minPitchAngle, maxPitchAngle);

            rotationPivot.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        }

        void HandleWeaponSelection()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                currentWeaponIndex = 0;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                currentWeaponIndex = 1;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                currentWeaponIndex = 2;
            }

            if (Input.mouseScrollDelta.y != lastMouseScrollDelta)
            {
                currentWeaponIndex += (int)Input.mouseScrollDelta.y;

                currentWeaponIndex = Mathf.Clamp(currentWeaponIndex, 0, inventory.Count - 1);

                lastMouseScrollDelta = (int)Input.mouseScrollDelta.y;
            }

            currentWeapon.Unequip();
            currentWeapon = inventory[currentWeaponIndex];
            currentWeapon.Equip();
        }

        public void TakeDamage(int amount)
        {
            health -= amount;

            if (OnDamageReceived != null)
                OnDamageReceived(amount);

            if (health <= 0)
            {
                Kill();
            }
        }

        public bool IsAlive()
        {
            return health > 0;
        }

        public void Kill()
        {
            health = 0;

            if (OnKilled != null)
                OnKilled();
        }

        public Weapon GetCurrentWeapon()
        {
            return currentWeapon;
        }
    }
}
