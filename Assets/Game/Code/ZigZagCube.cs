﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    public class ZigZagCube : Enemy
    {
        int currentTypeIndex = 0;
        bool isMovingLeft;
        bool isMovingRight;

        protected override Vector3 CalculateFlipPivotLocation()
        {
            currentTypeIndex = Random.Range(0, 10);

            if (currentTypeIndex < 3)
            {
                isMovingLeft = false;
                isMovingRight = true;
            }
            else if (currentTypeIndex < 6)
            {
                isMovingLeft = true;
                isMovingRight = false;
            }
            else
            {
                isMovingLeft = false;
                isMovingRight = false;
            }

            Vector3 pivotModifier = new Vector3(0, -1, 1);

            if (isMovingLeft || isMovingRight)
            {
                pivotModifier = new Vector3(isMovingLeft ? -1 : 1, -1, 0);
            }

            return transform.position + attachParent.TransformDirection(pivotModifier * Size * 0.5f);// new Vector3(-Size * 0.5f, -Size * 0.5f, 0.0f));
        }

        protected override void StartFlip()
        {
            base.StartFlip();

            if (isMovingLeft || isMovingRight)
                nextTargetRotationAngle = -90.0f;
        }

        protected override void HandleFlip()
        {
            if (rotationAccumulator >= 90.0f)
            {
                isRotating = false;

                Vector3 rot = spinPivot.rotation.eulerAngles;
                if (isMovingLeft || isMovingRight)
                {
                    rot.z = isMovingLeft ? -nextTargetRotationAngle : nextTargetRotationAngle;
                }
                else
                {
                    rot.x = nextTargetRotationAngle;
                }

                spinPivot.rotation = Quaternion.Euler(rot);

                transform.parent = attachParent;

                timeSinceLastFlip = 0.0f;
            }
            else
            {
                float rotationAngle = rotationSpeed * Time.deltaTime;

                if (isMovingLeft || isMovingRight)
                {
                    spinPivot.Rotate(Vector3.forward, isMovingLeft ? rotationAngle : -rotationAngle);
                }
                else
                {
                    spinPivot.Rotate(Vector3.right, rotationAngle);
                }

                rotationAccumulator += rotationAngle;
            }
        }
    }
}
