﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    public class JumpingCube : Enemy
    {
        protected override Vector3 CalculateFlipPivotLocation()
        {
            int jumpRandomCheck = Random.Range(1, 10);
            bool shouldJump = jumpRandomCheck > 3 && jumpRandomCheck < 6;

            float pivotOffset = (shouldJump ? 5 : 0.5f);

            return transform.position + attachParent.TransformDirection(new Vector3(0.0f, -Size * pivotOffset, Size * pivotOffset));
        }

        protected override void StartFlip()
        {
            base.StartFlip();
        }

        protected override void HandleFlip()
        {
            base.HandleFlip();
        }
    }
}
