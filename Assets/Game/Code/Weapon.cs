﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    [RequireComponent(typeof(ObjectPoolManager))]
    public class Weapon : MonoBehaviour
    {
        [SerializeField]
        protected int damage = 10;
        [SerializeField]
        protected float firingRate = 0.2f;
        [SerializeField]
        protected float spread = 0.1f;
        [SerializeField]
        protected int bulletsPerShot = 1;
        [SerializeField]
        protected string displayName = "Weapon";

        public GameObject projectilePrefab;

        private bool isEquipped = false;

        public bool autoRefire = true;

        private bool wantsToFire = false;
        private bool triggerPulled = false;

        private float timeSinceLastFire = 0.0f;

        private Vector3 lastHitPos;

        private ObjectPoolManager projectilePool;


        public string DisplayName
        {
            get { return displayName; }
        }


        void Start()
        {
            timeSinceLastFire = firingRate * 2;

            projectilePool = GetComponent<ObjectPoolManager>();
        }

        void Update()
        {
            if (!isEquipped)
                return;

            timeSinceLastFire += Time.deltaTime;
            if (timeSinceLastFire >= firingRate)
            {
                DoFire();
            }
        }

        public void Equip()
        {
            isEquipped = true;
        }

        public void Unequip()
        {
            isEquipped = false;
        }

        public void StartFiring()
        {
            if (!CanFire())
                return;

            triggerPulled = true;
            wantsToFire = true;
        }

        public void StopFiring()
        {
            triggerPulled = false;
            wantsToFire = false;
        }

        private void DoFire()
        {
            if (triggerPulled && wantsToFire)
            {
                for (int i = 0; i < bulletsPerShot; i++)
                {
                    ProcessFire();
                }

                timeSinceLastFire = 0.0f;

                if (!ShouldAutoRefire())
                {
                    wantsToFire = false;
                }
            }
        }

        private bool CanFire()
        {
            return true; // ammo check
        }

        private bool ShouldAutoRefire()
        {
            return autoRefire;
        }

        private void ProcessFire()
        {
            Vector3 weaponSpread = new Vector3(Random.Range(-spread, spread), Random.Range(-spread, spread), Random.Range(-spread, spread));

            Vector3 fireDirection = transform.TransformDirection(Vector3.forward);
            fireDirection.x += weaponSpread.x;
            fireDirection.y += weaponSpread.y;

            fireDirection.Normalize();

            projectilePool.Spawn(transform.position, Quaternion.LookRotation(fireDirection));

            RaycastHit hit;
            if (Physics.Raycast(transform.position, fireDirection, out hit))
            {
                Debug.DrawRay(transform.position, fireDirection * hit.distance, Color.yellow);

                if (hit.collider.gameObject.CompareTag("Enemy"))
                {
                    var enemy = hit.collider.gameObject.GetComponent<Enemy>();  
                    if (enemy.IsAlive())
                    {
                        enemy.TakeDamage(damage);
                    }
                }
            }
            else
            {
                Debug.DrawRay(transform.position, fireDirection * 1000, Color.white, 0.25f);
            }
        }
    }
}
