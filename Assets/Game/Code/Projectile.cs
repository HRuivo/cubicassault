﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubicAssault
{
    public class Projectile : MonoBehaviour
    {
        public float speed = 100.0f;
        public float lifetime = 5.0f;

        void Start()
        {
        }

        void Update()
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
    }
}
