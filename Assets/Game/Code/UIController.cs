﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public enum EScreenType
    {
        None,
        GameHUD,
        GameOver,
        Win
    };

    public Canvas gameOverScreenCanvas;
    public Canvas gameWinScreenCanvas;
    public Canvas GameHUDCanvas;


    public void SetCursorGameMode()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void SetCursorUIMode()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void HideAll()
    {
        GameHUDCanvas.enabled = false;
        gameOverScreenCanvas.enabled = false;
        gameWinScreenCanvas.enabled = false;
    }

    public void ShowScreen(EScreenType screenType)
    {
        HideAll();

        switch (screenType)
        {
            case EScreenType.GameHUD:
                GameHUDCanvas.enabled = true;
                break;
            case EScreenType.GameOver:
                gameOverScreenCanvas.enabled = true;
                break;
            case EScreenType.Win:
                gameWinScreenCanvas.enabled = true;
                break;
        }
    }

    public void HideScreen(EScreenType screenType)
    {
        switch (screenType)
        {
            case EScreenType.GameHUD:
                GameHUDCanvas.enabled = false;
                break;
            case EScreenType.GameOver:
                gameOverScreenCanvas.enabled = false;
                break;
            case EScreenType.Win:
                gameWinScreenCanvas.enabled = false;
                break;
        }
    }
}
