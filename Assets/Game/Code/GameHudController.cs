﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CubicAssault
{
    public class GameHudController : MonoBehaviour
    {
        public Text hpText;
        public Text killsCountdownText;
        public Text currentWeaponText;
        public Text currentCameraText;

        public Image damageFeedbackImage;

        public PlayerController playerController;
        public EnemySpawner enemySpawner;
        public PlayerCameraController cameraController;


        void Start()
        {
            if (playerController)
            {
                playerController.OnDamageReceived += PlayerController_OnDamageReceived;
            }
            else
            {
                Debug.LogError("No player controller set for the game hud controller.");
            }

            if (enemySpawner)
            {
                enemySpawner.OnCubeKilled += EnemySpawner_OnCubeKilled;
            }
            else
            {
                Debug.LogError("No enemy spawner set for the game hud controller.");
            }

            UpdatePlayerHealthDisplay();
            UpdateKillsCountdownDisplay();
            UpdateWeaponDisplay();
        }

        void Update()
        {
            UpdateWeaponDisplay();
            UpdateCameraDisplay();
        }

        private void EnemySpawner_OnCubeKilled()
        {
            UpdateKillsCountdownDisplay();
        }

        private void UpdatePlayerHealthDisplay()
        {
            if (hpText)
            {
                hpText.text = playerController.Health.ToString();
            }
        }

        private void UpdateWeaponDisplay()
        {
            currentWeaponText.text = playerController.GetCurrentWeapon().DisplayName;
        }

        private void UpdateCameraDisplay()
        {
            currentCameraText.text = cameraController.GetCurrentCameraName();
        }

        private void UpdateKillsCountdownDisplay()
        {
            if (killsCountdownText)
            {
                killsCountdownText.text = enemySpawner.GetNumKillsBeforeTitanSpawn().ToString();
            }
        }

        private void PlayerController_OnDamageReceived(int damage)
        {
            UpdatePlayerHealthDisplay();
            PlayDamageFeedback();
        }

        public void PlayDamageFeedback()
        {
            damageFeedbackImage.GetComponent<Animation>().Play();
        }
    }
}
