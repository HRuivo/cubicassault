﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
    [SerializeField]
    private int poolSize = 50;
    [SerializeField]
    private GameObject prefab;

    Queue<GameObject> poolQueue;

    void Start()
    {
        poolQueue = new Queue<GameObject>(poolSize);

        for (int i = 0; i < poolSize; i++)
        {
            GameObject newObject = Instantiate(prefab);
            newObject.transform.parent = transform;
            newObject.SetActive(false);

            poolQueue.Enqueue(newObject);
        }
    }

    public void Spawn(Vector3 position, Quaternion rotation)
    {
        GameObject gameObject = poolQueue.Dequeue();
        gameObject.transform.position = position;
        gameObject.transform.rotation = rotation;
        gameObject.SetActive(true);

        poolQueue.Enqueue(gameObject);
    }

    public void Remove()
    {

    }
}
